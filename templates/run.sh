#!/bin/bash

set -e

COMMAND="$1"
JENKINS_USER={{ jenkins_user }}
JENKINS_GROUP=jenkins
JENKINS_HOME={{ jenkins_home }}
JENKINS_WAR=${JENKINS_HOME}/jenkins.war
LOG_FILE=${JENKINS_HOME}/logs/jenkins.log
SERVICE_LOG_FILE=${JENKINS_HOME}/logs/jenkins.service.log
PID_FILE=${JENKINS_HOME}/.jenkins.pid
SERVICE_NAME='jenkins'
SERVICE_COMMAND="java -jar ${JENKINS_WAR} --httpPort={{ jenkins_port }}"

mkdir -p ${JENKINS_HOME}/logs

log() {
    DATE=$(date +'%Y-%m-%d @ %T')
    case $1 in
    "debug")
        echo -e "[$DATE]\tDEBUG:\t$2" >> $SERVICE_LOG_FILE
        ;;
    "info")
        echo -e "$2"
        echo -e "[$DATE]\tINFO:\t$2" >> $SERVICE_LOG_FILE
        ;;
    "error")
        echo -e "\e[31m${2}\e[0m"
        echo -e "[$DATE]\tERROR:\t\e[31m${2}\e[0m" >> $SERVICE_LOG_FILE
        ;;
    esac
}

error() {
    log 'error' "$1"
    exit 1
}

pid_file_exists() {
    log 'debug' "Checking PID File exists"
    if [[ -z "$PID_FILE" ]]; then
        if [[ -f $PID_FILE ]]; then
            log 'debug' "PID File does exist!"
            true
        fi
    fi
    false
}

purge_pid_file() {
    if pid_file_exists; then
        log 'info' "Deleting PID File"
        rm -f $PID_FILE
    fi

    if [[ ! -f $PID_FILE ]]; then
        log 'debug' "Resetting PID_FILE variable"
        PID_FILE=
    fi
}

stop_service() {
    if pid_file_exists; then
        log 'info' "Stopping $SERVICE_NAME service..."
        kill $(cat $PID_FILE)
    else
        error "$SERVICE_NAME service is already stopped!"
    fi
}

start_service() {
    if [ $EUID -eq 0 ]; then
        error "Will not start service as root!"
    fi
    if ! pid_file_exists; then
        log 'info' "Starting $SERVICE_NAME service..."
        log 'debug' "Running command: '$SERVICE_COMMAND' as '$(whoami)'"
        nohup $SERVICE_COMMAND < /dev/null > $LOG_FILE 2>&1 &
        echo -n "$!" > $PID_FILE

        sleep 5

        if [ ! -f $PID_FILE ]; then
            error "Failed to start ${SERVICE_NAME}!"
        fi

        if [ -z "$(ps -aux | grep jenkins | grep -v 'grep')" ]; then
            error "Process is not running?!"
        fi
    else
        error "$SERVICE_NAME service is already running!"
    fi
}

finish() {
    if [ $? -ne 0 ]; then
        error "Something went wrong? ($@)"
    fi
}

trap finish EXIT
trap finish SIGINT

case $COMMAND in
"start")
    start_service
    ;;
"stop")
    stop_service
    purge_pid_file
    ;;
"restart")
    stop_service
    start_service
    ;;
*)
    error "Please provide action $0 <start|stop|restart>"
    ;;
esac