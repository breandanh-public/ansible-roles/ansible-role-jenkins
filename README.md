# Java

Installs Jenkins CI.

## Requirements

None.

## Role Variables

* __jenkins_home__: Jenkins home directory \[Default: `/home/jenkins`\]
* __jenkins_port__: Jenkins port \[Default: `8080`\]
* __jenkins_user__: Jenkins user \[Default: `jenkins`\]

## Dependencies

None.

## Example Playbook

```
- hosts: servers
  roles:
     - role: jenkins
       vars:
         jenkins_home: /home/jenkins
         jenkins_port: 8080
         jenkins_user: jenkins
```


## License

Apache 2.0

## Author Information

BreandanH
